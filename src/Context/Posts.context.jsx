import axios from "axios";
import { useState, createContext } from "react";

export const PostsContext = createContext({
  posts: [],
  setPosts: () => null,
  activePost: [],
  setActivePost: () => null,
});

export const PostsProvider = ({ children }) => {
  const [posts, setPosts] = useState([]);
  const [activePost, setActivePost] = useState(null);

  const getPosts = (offset) => {
    // get posts by givven offset
    axios
      .get(
        `${process.env.REACT_APP_API_URL}posts?offset=${offset}&limit=20&api_key=${process.env.REACT_APP_API_KEY}`
      )
      .then(({ data }) => {
        if (data.success) {
          setPosts(data.response.posts);
        }
      })
      .catch((err) => {
        console.error(err);
      });
  };

  const getActivePostData = (post) => {
    // fetch media metadata
    const media = axios.get(
      `${process.env.REACT_APP_API_URL}medias/${post.mediaId}?api_key=${process.env.REACT_APP_API_KEY}`
    );
    // fetch user information
    const user = axios.get(
      `${process.env.REACT_APP_API_URL}users/${post.user.username}?api_key=${process.env.REACT_APP_API_KEY}`
    );

    // wait for data and set the post
    Promise.all([media, user])
      .then(([media, user]) => {
        setActivePost({
          post: post,
          media: media.data.response.media.urls,
          user: user.data.response.user,
        });
      })
      .catch((error) => console.error(error));
  };

  const value = {
    posts,
    getPosts,
    activePost,
    getActivePostData,
  };
  return (
    <PostsContext.Provider value={value}>{children}</PostsContext.Provider>
  );
};
