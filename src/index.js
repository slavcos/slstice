import React from "react";
import ReactDOM from "react-dom/client";
// import { PostsProvider } from "./context/Posts.context";
import App from "./App";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  //   <PostsProvider>
  <App />
  //   </PostsProvider>
);
