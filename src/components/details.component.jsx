import moment from "moment";

import Like from "../assets/images/like.svg";

const Details = ({ post }) => {
  return (
    <div className="details">
      <div className="userDetails">
        <p>
          {post.user.first_name} {post.user.last_name ?? ""}
        </p>
        <div className="profileImage">
          <img src={post.user.profile_images.small} alt="profile" />
        </div>
      </div>
      <div className="imageDescription">
        <h1>{post.post.title}</h1>
        <h3>{post.post.description}</h3>
      </div>
      <div className="bottom">
        <div className="likes">
          <img src={Like} alt="Like" width={24} />
          <span>{post.post.likes} persons</span>
        </div>
        <div className="datePosted">
          <span>{moment(post.post.created).fromNow()}</span>
        </div>
      </div>
    </div>
  );
};

export default Details;
