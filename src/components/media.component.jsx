const Media = ({ post }) => {
  return (
    <div className="imageWrap">
      <div
        className="blurImage"
        style={{
          backgroundImage: `url(${post.media.regular})`,
        }}
      ></div>
      <img src={post.media.small} alt={post.title} />
    </div>
  );
};

export default Media;
