import { Fragment, useEffect, useState, useCallback, useContext } from "react";
import axios from "axios";

import "./assets/css/Style.scss";
import Media from "./components/Media.component";
import Details from "./components/Details.component";
import { getPosts } from "./Services/getPosts";
import { getPostData } from "./Services/getPostData";
// import { PostsContext } from "./context/Posts.context";

function App() {
  //   const { posts, getPosts, activePost, getActivePostData } = useContext(PostsContext);
  const [posts, setPosts] = useState([]);
  const [activePost, setActivePost] = useState();
  const [counter, setCounter] = useState(0);

  useEffect(() => {
    // get the every next 20 posts
    if (counter === posts.length - 1) setCounter(0);

    if (counter === 0) {
      getPosts(posts.length).then((allPosts) => setPosts(allPosts));
    }
  }, [counter]);

  useEffect(() => {
    // set the new post every 6 seconds
    if (posts.length > 0) {
      getPostData(posts[counter]).then((postActive) =>
        setActivePost(postActive)
      );

      const interval = setInterval(() => {
        setCounter((count) => count + 1);
      }, 6000);
      return () => clearInterval(interval);
    }
  }, [counter, posts]);

  return (
    <div className="container">
      {activePost ? (
        <Fragment>
          <Media post={activePost} />
          <Details post={activePost} />
        </Fragment>
      ) : (
        <div className="loader">Please wait ... </div>
      )}
    </div>
  );
}

export default App;
