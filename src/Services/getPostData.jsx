import axios from "axios";

export const getPostData = async (post) => {
  // fetch media metadata
  const media = await axios.get(
    `${process.env.REACT_APP_API_URL}medias/${post.mediaId}?api_key=${process.env.REACT_APP_API_KEY}`
  );
  // fetch user information
  const user = await axios.get(
    `${process.env.REACT_APP_API_URL}users/${post.user.username}?api_key=${process.env.REACT_APP_API_KEY}`
  );

  return {
    post: post,
    media: media.data.response.media.urls,
    user: user.data.response.user,
  };
};
