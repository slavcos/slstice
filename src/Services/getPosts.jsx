import axios from "axios";

export const getPosts = async (offset) => {
  // get posts by givven offset
  let post = await axios.get(
    `${process.env.REACT_APP_API_URL}posts?offset=${offset}&limit=20&api_key=${process.env.REACT_APP_API_KEY}`
  );

  return post.data.response.posts;
};
